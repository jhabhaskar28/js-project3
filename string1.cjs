function string1(testString){
    if(testString == undefined || testString.constructor != String){
        return 0;
    }
    let isNegative = false;
    if(testString[0] == '-' && testString[1] == '$'){
        isNegative = true;
        testString = testString.substring(2);
    } else if(testString[0] == '$'){
        testString = testString.substring(1);
    } else {
        return 0;
    }
    testString = testString.replaceAll(',','');
    let decimalCount = 0;
    let numberString = "";
    for(let index=0;index<testString.length;index++){
        if(testString[index] == '.'){
            decimalCount ++;
            if(decimalCount > 1){
                return 0;
            }
            numberString += testString[index];
        } else if(parseInt(testString[index])>=0 && parseInt(testString[index])<=9){
            numberString += testString[index];
        } else{
            return 0;
        }
    }
    let result = parseFloat(numberString);
    if(isNegative == true){
        result = -result;
    }
    return result;
}

module.exports = string1;