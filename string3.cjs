function string3(testString){
    if(testString == undefined || testString.constructor != String || testString == ""){
        return "";
    }
    let monthsArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let month = parseInt(testString.split('/')[1]);
    if(month>12 || month<1){
        return "";
    }
    let result = monthsArray[month-1];
    return result;
}

module.exports = string3;