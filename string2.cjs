function string2(testString){
    if(testString == undefined || testString.constructor != String){
        return [];
    }
    let dotCount = 0;
    for(let index=0;index<testString.length;index++){
        if(testString.charAt(index) == '.'){
            dotCount++;
        }
    }
    if(dotCount == 3){
        let stringArray = testString.split('.');
        let resultArray = [];
        for(let index = 0; index<stringArray.length;index++){
            resultArray.push(parseInt(stringArray[index]));
        }
        for(let index = 0;index<resultArray.length;index++){
            if(resultArray[index]>255 || resultArray[index]<0){
                return [];
            }
        }
        return resultArray;
    } else {
        return [];
    }
}
module.exports = string2;