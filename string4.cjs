function string4(testObject){
    if(testObject == undefined || testObject.constructor != Object || typeof(testObject)!= 'object' || Object.keys(testObject).length == 0){
        return "";
    }
    let result = new String("");
    for(index in testObject){
        let name = testObject[index];
        name = name.toLowerCase();
        name = name[0].toUpperCase() + name.substring(1) + " ";
        result += name;
    }
    result = result.trimEnd();
    return result;
}

module.exports = string4;