function string5(testArray){
    if(testArray == undefined || Array.isArray(testArray)==false || testArray.length == 0){
        return "";
    }
    for(let index = 0; index<testArray.length ; index++){
        if(testArray[index].constructor != String){
            return "";
        }
    }
    let result = testArray[0];
    for(let index=1;index<testArray.length;index++){
        result = result.concat(' ', testArray[index]);
    }
    result += ".";
    return result;
}

module.exports = string5;